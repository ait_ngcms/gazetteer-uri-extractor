package at.ait;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CsvProcessing {

	static String absoluteBasePath = "";
	static String inputDir = absoluteBasePath + File.separator + "csvs";
	static String outputDir = absoluteBasePath + File.separator + "csvs_processed";

	public static void main(String[] args) {
		extractGazetteerUris(inputDir);
	}

	public static void extractGazetteerUris(String directory) {
		List<String> gazetteerUriList = new ArrayList<>();
		try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(Paths.get(directory))) {
			for (Path path : directoryStream) {
				System.out.println("Processing file: " + path.getFileName());
				BufferedReader reader = Files.newBufferedReader(path, Charset.forName("UTF-8"));
				String line = null;
				while ((line = reader.readLine()) != null) {
					String[] entries = line.split(";");
					if (entries.length > 2) {
						String status = entries[2];
						String gazetteerUri = entries.length > 5 ? entries[5] : "";
						String gazetteerUriCorrected = entries.length > 10 ? entries[10] : "";
						if (status.equals("VERIFIED")) {
							if (!gazetteerUriCorrected.isEmpty())
								gazetteerUriList.add(gazetteerUriCorrected);
							else if (!gazetteerUri.isEmpty())
								gazetteerUriList.add(gazetteerUri);
							else
								System.out.println("ignoring invalid line");
						}
					}
				}
				reader.close();
				Collections.sort(gazetteerUriList);
				Files.createDirectories(Paths.get(outputDir));
				PrintWriter writer = new PrintWriter(outputDir + File.separator + path.getFileName(), "UTF-8");
				for (String gazetteerUri : gazetteerUriList)
					writer.println(gazetteerUri);
				
				writer.close();
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
