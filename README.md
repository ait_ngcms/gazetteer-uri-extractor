# Gazetteer URI Extractor #

This tool extracts verified gazetteer URIs from csv data of pelagios objects. This information is intended to perform a similarity analysis based on context triggered piecewise hashing (also known as fuzzy hashing).

### Usage ###

* The input and output directories have to be adjusted in the source code or taken as is (input: csvs, output: csvs_processed).
* To calculate hashes and compare the similarity the tool [ssdeep](http://ssdeep.sourceforge.net) can be used.

search for matches:

        cd csvs_processed
        [path]\ssdeep -pbc *

compute hashes only:

        [path]\ssdeep *